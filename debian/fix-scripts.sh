#!/bin/sh

find debian/tmp/usr/bin -name \*.py | while read f
do
    echo $f:
    head -n1 $f
    if head -n1 "$f" | grep python3; then
        mv "$f" "$f-python3"
    else
        mv "$f" "$f-python2"
    fi
done
